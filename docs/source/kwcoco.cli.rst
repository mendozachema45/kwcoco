kwcoco.cli package
==================

Submodules
----------

.. toctree::
   :maxdepth: 4

   kwcoco.cli.coco_conform
   kwcoco.cli.coco_eval
   kwcoco.cli.coco_grab
   kwcoco.cli.coco_modify_categories
   kwcoco.cli.coco_reroot
   kwcoco.cli.coco_show
   kwcoco.cli.coco_split
   kwcoco.cli.coco_stats
   kwcoco.cli.coco_subset
   kwcoco.cli.coco_toydata
   kwcoco.cli.coco_union
   kwcoco.cli.coco_validate

Module contents
---------------

.. automodule:: kwcoco.cli
   :members:
   :undoc-members:
   :show-inheritance:
