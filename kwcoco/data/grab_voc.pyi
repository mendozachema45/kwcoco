from typing import Union
from typing import Dict
from _typeshed import Incomplete


def convert_voc_to_coco(dpath: Incomplete | None = ...):
    ...


def ensure_voc_data(dpath: Incomplete | None = ...,
                    force: bool = ...,
                    years=...):
    ...


def ensure_voc_coco(dpath: Union[str, None] = None) -> Dict[str, str]:
    ...


def main() -> None:
    ...
